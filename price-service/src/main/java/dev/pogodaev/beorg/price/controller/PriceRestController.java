package dev.pogodaev.beorg.price.controller;

import dev.pogodaev.beorg.common.entity.reqresp.SecondRequestDTO;
import dev.pogodaev.beorg.common.entity.reqresp.ThirdRequestDTO;
import dev.pogodaev.beorg.common.feign.pricefeing.PriceServiceInterface;
import dev.pogodaev.beorg.price.entity.Price;
import dev.pogodaev.beorg.repository.jpaRepo.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@RestController
public class PriceRestController implements PriceServiceInterface {

    @Autowired
    private PriceRepository priceRepository;

    private static final String OK = "OK";
    private static final String ERROR = "ERROR";

    @Override
    @RequestMapping(value = "/write")
    public
    @ResponseBody
    String write(@RequestBody ThirdRequestDTO req) {
        Price p = priceRepository.save(new Price(req.getPid(), req.getAmount()));
        return (p != null) ? OK : ERROR;
    }

    @Override
    public
    @ResponseBody
    SecondRequestDTO responsePrice(@RequestBody String productId) {

        BigDecimal sum = new BigDecimal(0);
        BigDecimal avg = new BigDecimal(0);
        List<Price> list = priceRepository.findByIdProduct(productId);

        if (list != null && list.size() > 0) {
            for (Price price : list) {
                sum = sum.add(price.getPrice());
            }
            avg = sum.divide(new BigDecimal(list.size()), 3, RoundingMode.CEILING);
        }

        SecondRequestDTO secondRequestDTO = new SecondRequestDTO();
        secondRequestDTO.setAvg(avg);
        return secondRequestDTO;
    }
}
