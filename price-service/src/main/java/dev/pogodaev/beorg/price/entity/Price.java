package dev.pogodaev.beorg.price.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "prices")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idPrice;
    private String idProduct;
    private BigDecimal price;

    public Price() {
    }

    public Price(String idProduct, BigDecimal price) {
        this.idProduct = idProduct;
        this.price = price;
    }

    public Long getIdPrice() {
        return idPrice;
    }

    public void setIdPrice(Long idPrice) {
        this.idPrice = idPrice;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
