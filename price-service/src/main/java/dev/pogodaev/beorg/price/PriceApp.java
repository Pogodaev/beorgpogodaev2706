package dev.pogodaev.beorg.price;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "dev.pogodaev.beorg.repository.jpaRepo")
@EntityScan(basePackages = "dev.pogodaev.beorg.price.entity")
@SpringBootApplication(scanBasePackages = {"dev.pogodaev.beorg.common.feign.pricefeing", "dev.pogodaev.beorg.price"})
public class PriceApp implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PriceApp.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

    }
}
