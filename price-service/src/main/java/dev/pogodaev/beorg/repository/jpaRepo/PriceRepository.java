package dev.pogodaev.beorg.repository.jpaRepo;

import dev.pogodaev.beorg.price.entity.Price;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PriceRepository extends CrudRepository<Price, Long> {

    List<Price> findByIdProduct(String idProduct);
}
