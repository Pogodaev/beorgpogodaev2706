package dev.pogodaev.beorg.mongoRepo;

import dev.pogodaev.beorg.common.entity.productservice.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {

    Product findFirstByName(String name);
}
