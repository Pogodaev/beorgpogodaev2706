package dev.pogodaev.beorg.api;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"dev.pogodaev.beorg.api", "dev.pogodaev.beorg.common.feign.feingAPI", "dev.pogodaev.beorg.mongoRepo"})
public class APIMain implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(APIMain.class);
    }

    @Override
    public void run(String... strings) throws Exception {
    }
}
