package dev.pogodaev.beorg.api;

import dev.pogodaev.beorg.common.entity.productservice.Product;
import dev.pogodaev.beorg.common.entity.reqresp.FirstRequestDTO;
import dev.pogodaev.beorg.common.entity.reqresp.SecondRequestDTO;
import dev.pogodaev.beorg.common.entity.reqresp.ThirdRequestDTO;
import dev.pogodaev.beorg.common.feign.feingAPI.APIFeign;
import dev.pogodaev.beorg.common.feign.feingAPI.APIInterface;
import dev.pogodaev.beorg.mongoRepo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@EnableMongoRepositories(basePackages = "dev.pogodaev.beorg.mongoRepo")
@RestController
public class APIRestController implements APIInterface {

    @Autowired
    private APIFeign apiFeign;

    @Autowired
    private ProductRepository productRepository;

    private final static String ERROR = "ERROR";
    private final static String OK = "OK";

    @RequestMapping(value = "/test")
    public
    @ResponseBody
    String test() {
        FirstRequestDTO firstRequestDTO = new FirstRequestDTO("egg", new BigDecimal("600.04"));
        String st = externalRequest(firstRequestDTO);
        return st;
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/product")
    public
    @ResponseBody
    String externalRequest(@RequestBody FirstRequestDTO request) {

        String result = "";
        String name = request.getName();
        BigDecimal price = request.getPrice();

        Product res = productRepository.findFirstByName(name);

        if (res != null) {
            SecondRequestDTO responsePrice = apiFeign.getPriceService().responsePrice(res.getId());
            result = String.valueOf(responsePrice.getAvg());

            BigDecimal avg = new BigDecimal(result);
            BigDecimal bottom = avg.multiply(new BigDecimal(0.3));
            BigDecimal top = avg.multiply(new BigDecimal(1.7));

            if (price.compareTo(bottom) < 0 && price.compareTo(top) > 0) {
                return ERROR;
            } else {

                ThirdRequestDTO thirdRequestDTO = new ThirdRequestDTO();
                thirdRequestDTO.setAmount(price);
                thirdRequestDTO.setPid(res.getId());
                System.out.println("Hello");
                String rest = apiFeign.getPriceService().write(thirdRequestDTO);

                System.out.println("Yeeee");
                System.out.println(rest);
                return rest;
            }
        } else {
            Product product = productRepository.save(new Product(name)); //new Product

            ThirdRequestDTO thirdRequestDTO = new ThirdRequestDTO();
            thirdRequestDTO.setAmount(price);
            thirdRequestDTO.setPid(res.getId());
            String writeResult = apiFeign.getPriceService().write(thirdRequestDTO); // REQ3 - >

            if (writeResult.equals(ERROR)) {
                productRepository.delete(product);
            }
            return (writeResult.equals(ERROR)) ? ERROR : OK;
        }
    }
}
