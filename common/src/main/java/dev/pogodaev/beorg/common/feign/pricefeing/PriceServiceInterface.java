package dev.pogodaev.beorg.common.feign.pricefeing;

import dev.pogodaev.beorg.common.entity.reqresp.SecondRequestDTO;
import dev.pogodaev.beorg.common.entity.reqresp.ThirdRequestDTO;
import feign.Headers;
import feign.RequestLine;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

public interface PriceServiceInterface {
    @Headers({"Content-Type: application/json"})
    @RequestLine("POST /write")
    @ResponseBody
    String write(@RequestBody ThirdRequestDTO req);

    @RequestMapping(method = RequestMethod.POST, value = "/third")
    @RequestLine("POST /third")
    @ResponseBody
    SecondRequestDTO responsePrice(@RequestBody String productId);
}
