package dev.pogodaev.beorg.common;

import dev.pogodaev.beorg.common.entity.reqresp.FirstRequestDTO;
import dev.pogodaev.beorg.common.feign.external.ExternalFeign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;

//import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * Be aware it's a test App and it works with real databases start with empty bases
 */

//@EnableFeignClients
@SpringBootApplication
public class MainTestClass implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(MainTestClass.class);
    private static final String PUTTING_RESULT = "Putting result for ";
    private static final String OK_TEST = "OK test for ";
    private static final String ERROR_TEST = "ERROR test for ";

    @Autowired
    private ExternalFeign externalFeign;

    public static void main(String[] args) {
        SpringApplication.run(MainTestClass.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        /*Let's create new Values*/
        String resultEgg = externalFeign.externalRequest(new FirstRequestDTO("egg", new BigDecimal("100.004")));
        logger.info(PUTTING_RESULT + " egg : " + resultEgg);
        String resultSpoon = externalFeign.externalRequest(new FirstRequestDTO("spoon", new BigDecimal("1000.004")));
        logger.info(PUTTING_RESULT + " spoon: " + resultSpoon);
        String resultDish = externalFeign.externalRequest(new FirstRequestDTO("dish", new BigDecimal("10000.004")));
        logger.info(PUTTING_RESULT + " dish: " + resultDish);
        String testEggOk = externalFeign.externalRequest(new FirstRequestDTO("egg", new BigDecimal("80.067")));
        logger.info(OK_TEST + "egg: " + testEggOk);

        String testEggError = externalFeign.externalRequest(new FirstRequestDTO("egg", new BigDecimal("240.067")));
        logger.info(ERROR_TEST + "egg: " + testEggError);
    }
}
