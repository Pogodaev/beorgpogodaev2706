package dev.pogodaev.beorg.common.entity.reqresp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class FirstRequestDTO implements Serializable {
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private BigDecimal price;

    public FirstRequestDTO(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
