package dev.pogodaev.beorg.common.entity.productservice;

public class Product {

    private String id;
    private String name;

    public Product() {
    }

    public Product(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format(
                "Product[id=%s, name='%s']",
                id, name);
    }
}
