package dev.pogodaev.beorg.common.feign.external;

import dev.pogodaev.beorg.common.feign.feingAPI.APIInterface;
import org.springframework.stereotype.Component;

@Component
public interface ExternalFeign extends APIInterface {

}
