package dev.pogodaev.beorg.common.entity.reqresp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class SecondRequestDTO implements Serializable {

    @JsonProperty("avg")
    private BigDecimal avg;

    public SecondRequestDTO() {
    }

    public BigDecimal getAvg() {
        return avg;
    }

    public void setAvg(BigDecimal avg) {
        this.avg = avg;
    }
}
