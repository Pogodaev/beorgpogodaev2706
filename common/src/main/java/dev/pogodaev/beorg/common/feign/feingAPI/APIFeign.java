package dev.pogodaev.beorg.common.feign.feingAPI;

import dev.pogodaev.beorg.common.feign.pricefeing.PriceServiceInterface;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.stereotype.Component;

@Component
public class APIFeign {
    private static final String URI_AVG = "http://localhost:8095";

    private PriceServiceInterface priceService;

    public APIFeign() {
        priceService = Feign.builder().encoder(new JacksonEncoder()).decoder(new JacksonDecoder()).target(PriceServiceInterface.class, URI_AVG);
    }

    public PriceServiceInterface getPriceService() {
        return priceService;
    }

    public void setPriceService(PriceServiceInterface priceService) {
        this.priceService = priceService;
    }
}
