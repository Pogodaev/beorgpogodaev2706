package dev.pogodaev.beorg.common.feign.feingAPI;

import dev.pogodaev.beorg.common.entity.reqresp.FirstRequestDTO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public interface APIInterface {

    @ResponseBody
    String externalRequest(@RequestBody FirstRequestDTO request);
}
