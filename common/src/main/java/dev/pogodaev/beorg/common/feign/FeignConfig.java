package dev.pogodaev.beorg.common.feign;

import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {
    private static final int TEN_SECONDS = 10000;

    @Bean
    public feign.Logger.Level feignLoggerLevel() {
        return feign.Logger.Level.FULL;
    }

    @Bean
    public Request.Options options() {
        return new Request.Options(TEN_SECONDS, TEN_SECONDS);
    }
}
