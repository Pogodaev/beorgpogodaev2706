package dev.pogodaev.beorg.common.entity.reqresp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class ThirdRequestDTO implements Serializable {
    @JsonProperty("amount")
    private BigDecimal amount;
    @JsonProperty("pid")
    private String pid;

    public ThirdRequestDTO() {
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
